package net.pl3x.bukkit.campfires.listener;

import net.pl3x.bukkit.campfires.Campfires;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;

public class CampfireListener implements Listener {
    private final Campfires plugin;

    public CampfireListener(Campfires plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onCampfirePlace(EntitySpawnEvent event) {
        plugin.getServer().getScheduler().runTaskLater(plugin, () -> {
            if (plugin.getNewItems().isCampfire(event.getEntity())) {
                plugin.getTracker().add((ArmorStand) event.getEntity());
            }
        }, 1);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onChunkLoad(ChunkLoadEvent event) {
        plugin.getServer().getScheduler().runTaskLater(plugin, () -> {
            for (Entity entity : event.getChunk().getEntities()) {
                if (plugin.getNewItems().isCampfire(entity)) {
                    plugin.getTracker().add((ArmorStand) entity);
                }
            }
        }, 1);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onChunkUnload(ChunkUnloadEvent event) {
        // DO NOT schedule this (or it will fall through floor)
        for (Entity entity : event.getChunk().getEntities()) {
            if (plugin.getNewItems().isCampfire(entity)) {
                plugin.getTracker().remove((ArmorStand) entity);
            }
        }
    }

    /*@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPistonPush(BlockPistonExtendEvent event) {
        for (Block block : event.getBlocks()) {
            if (plugin.getTracker().contains(block.getLocation())) {
                event.setCancelled(true);
                return;
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPistonPull(BlockPistonRetractEvent event) {
        for (Block block : event.getBlocks()) {
            if (plugin.getTracker().contains(block.getLocation())) {
                event.setCancelled(true);
                return;
            }
        }
    }*/
}
