package net.pl3x.bukkit.campfires.hook;

import a.a.a.b;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Field;

public class NewItems {
    public boolean isCampfire(Entity entity) {
        if (entity.getType() != EntityType.ARMOR_STAND) {
            return false; // not an armor stand
        }
        ItemStack item = ((ArmorStand) entity).getEquipment().getHelmet();
        return isItem(item) && item.getItemMeta().getDisplayName().equals("\u00a7fCampfire");
    }

    private boolean isItem(ItemStack itemStack) {
        if (itemStack == null || itemStack.getType() != Material.DIAMOND_HOE) {
            return false;
        }
        b var2 = new b(itemStack);
        return var2.hasKey("NItype") && var2.getString("NItype").equals("item");
    }

    private <T> T getOfT(Object obj, Class<T> type) {
        for (Field field : obj.getClass().getDeclaredFields()) {
            if (type.equals(field.getType())) {
                return get(obj, field, type);
            }
        }
        return null;
    }

    private <T> T get(Object obj, Field field, Class<T> type) {
        try {
            field.setAccessible(true);
            return type.cast(field.get(obj));
        } catch (ReflectiveOperationException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
