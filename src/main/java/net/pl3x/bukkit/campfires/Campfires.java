package net.pl3x.bukkit.campfires;

import net.pl3x.bukkit.campfires.campfire.BurnTask;
import net.pl3x.bukkit.campfires.campfire.LightTask;
import net.pl3x.bukkit.campfires.campfire.RegenTask;
import net.pl3x.bukkit.campfires.campfire.Tracker;
import net.pl3x.bukkit.campfires.configuration.Config;
import net.pl3x.bukkit.campfires.configuration.Lang;
import net.pl3x.bukkit.campfires.hook.NewItems;
import net.pl3x.bukkit.campfires.listener.CampfireListener;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.plugin.java.JavaPlugin;

public class Campfires extends JavaPlugin {
    private final Logger logger;
    private Tracker tracker;
    private NewItems newItems;
    private LightTask lightTask;
    private RegenTask regenTask;
    private BurnTask burnTask;

    public Campfires() {
        this.logger = new Logger(this);
        this.tracker = new Tracker();
    }

    public Logger getLog() {
        return logger;
    }

    @Override
    public void onEnable() {
        Config.reload(this);
        Lang.reload(this);

        if (!getServer().getPluginManager().isPluginEnabled("NewItems")) {
            getLog().error("NewItems plugin is NOT loaded!");
            getLog().error("Disabling...");
            return;
        }

        newItems = new NewItems();

        lightTask = new LightTask(this);
        lightTask.runTaskTimer(this, 10, 10);

        regenTask = new RegenTask(this);
        regenTask.runTaskTimer(this, 50, 50);

        burnTask = new BurnTask(this);
        burnTask.runTaskTimer(this, 5, 5);

        for (World world : getServer().getWorlds()) {
            for (Chunk chunk : world.getLoadedChunks()) {
                for (Entity entity : chunk.getEntities()) {
                    if (newItems.isCampfire(entity)) {
                        tracker.add((ArmorStand) entity);
                    }
                }
            }
        }

        getServer().getPluginManager().registerEvents(new CampfireListener(this), this);

        getLog().info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        if (lightTask != null) {
            lightTask.cancel();
            lightTask = null;
        }

        if (regenTask != null) {
            regenTask.cancel();
            regenTask = null;
        }

        if (burnTask != null) {
            burnTask.cancel();
            burnTask = null;
        }

        if (tracker != null) {
            tracker.clear();
            tracker = null;
        }

        getLog().info(getName() + " disabled.");
    }

    public Tracker getTracker() {
        return tracker;
    }

    public NewItems getNewItems() {
        return newItems;
    }
}
