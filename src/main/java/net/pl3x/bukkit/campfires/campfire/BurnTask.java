package net.pl3x.bukkit.campfires.campfire;

import net.pl3x.bukkit.campfires.Campfires;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashSet;

public class BurnTask extends BukkitRunnable {
    private final Campfires plugin;

    public BurnTask(Campfires plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {
        for (ArmorStand armorStand : new HashSet<>(plugin.getTracker().getCampfires())) {
            if (armorStand == null || armorStand.isDead()) {
                continue;
            }
            for (Player player : armorStand.getWorld().getPlayers()) {
                if (armorStand.getLocation().distanceSquared(player.getLocation()) <= 0.65) {
                    player.removePotionEffect(PotionEffectType.REGENERATION);
                    player.setFireTicks(200);
                }
            }
        }
    }
}
