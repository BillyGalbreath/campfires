package net.pl3x.bukkit.campfires.campfire;

import net.minecraft.server.v1_12_R1.BlockPosition;
import net.minecraft.server.v1_12_R1.Chunk;
import net.minecraft.server.v1_12_R1.EntityHuman;
import net.minecraft.server.v1_12_R1.EntityPlayer;
import net.minecraft.server.v1_12_R1.EnumSkyBlock;
import net.minecraft.server.v1_12_R1.PacketPlayOutMapChunk;
import net.minecraft.server.v1_12_R1.WorldServer;
import net.pl3x.bukkit.campfires.Campfires;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.entity.ArmorStand;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class LightTask extends BukkitRunnable {
    private final Campfires plugin;
    private Field cachedChunkModified;
    private Map<BlockPosition, Integer> alreadyLit = new HashMap<>();

    public LightTask(Campfires plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {
        Set<Chunk> modifiedChunks = new HashSet<>();
        for (ArmorStand armorStand : new HashSet<>(plugin.getTracker().getCampfires())) {
            Location loc = armorStand.getLocation();
            WorldServer nmsWorld = ((CraftWorld) armorStand.getWorld()).getHandle();
            BlockPosition pos = new BlockPosition(loc.getBlockX(), loc.getBlockY() + 2, loc.getBlockZ());
            Block block = loc.clone().add(0, 1, 0).getBlock();
            if (armorStand.isDead()) {
                plugin.getTracker().remove(armorStand);
                alreadyLit.remove(pos);

                // recalculate
                nmsWorld.c(EnumSkyBlock.BLOCK, pos);
                force(block, Material.STONE);
            } else {
                Integer count = alreadyLit.get(pos);
                if (count == null) {
                    count = 0;
                } else if (count > 1) {
                    continue;
                }
                alreadyLit.put(pos, ++count);

                // create light
                nmsWorld.a(EnumSkyBlock.BLOCK, pos, 15);
                force(block, Material.FIRE);
            }
            modifiedChunks.addAll(getModifiedChunks(nmsWorld, pos.getX(), pos.getZ()));
        }
        for (Chunk chunk : modifiedChunks) {
            sendChunkUpdate(chunk);
        }
    }

    private void sendChunkUpdate(Chunk chunk) {
        for (EntityHuman player : chunk.getWorld().players) {
            Chunk pChunk = player.world.getChunkAtWorldCoords(player.getChunkCoordinates());
            if (distanceToSquared(pChunk, chunk) < 25) { // 5 chunk square radius
                PacketPlayOutMapChunk packet = new PacketPlayOutMapChunk(chunk, 65534);
                ((EntityPlayer) player).playerConnection.sendPacket(packet);
            }
        }
    }

    private int distanceToSquared(Chunk from, Chunk to) {
        if (!from.world.getWorldData().getName().equals(to.world.getWorldData().getName())) {
            return 100;
        }
        double var2 = to.locX - from.locX;
        double var4 = to.locZ - from.locZ;
        return (int) (var2 * var2 + var4 * var4);
    }

    private Collection<Chunk> getModifiedChunks(WorldServer nmsWorld, int x, int z) {
        Collection<Chunk> set = new HashSet<>();
        int chunkX = x >> 4;
        int chunkZ = z >> 4;
        try {
            for (int dX = -1; dX <= 1; dX++) {
                for (int dZ = -1; dZ <= 1; dZ++) {
                    if (nmsWorld.getChunkProviderServer().isLoaded(chunkX + dX, chunkZ + dZ)) {
                        Chunk chunk = nmsWorld.getChunkAt(chunkX + dX, chunkZ + dZ);
                        Field isModified = getChunkField(chunk);
                        if (isModified.getBoolean(chunk)) {
                            set.add(chunk);
                            chunk.f(false);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return set;
    }

    private Field getChunkField(Object chunk) throws NoSuchFieldException, SecurityException {
        if (cachedChunkModified == null) {
            cachedChunkModified = chunk.getClass().getDeclaredField("s");
            cachedChunkModified.setAccessible(true);
        }
        return cachedChunkModified;
    }

    private void force(Block block, Material to) {
        Material material = block.getType();
        byte data = block.getData();
        block.setType(to);
        block.setType(material);
        block.setData(data);
    }
}