package net.pl3x.bukkit.campfires.campfire;

import net.pl3x.bukkit.campfires.Campfires;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashSet;

public class RegenTask extends BukkitRunnable {
    private final Campfires plugin;

    public RegenTask(Campfires plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {
        for (ArmorStand armorStand : new HashSet<>(plugin.getTracker().getCampfires())) {
            if (armorStand == null || armorStand.isDead()) {
                continue;
            }
            for (Player player : armorStand.getWorld().getPlayers()) {
                if (armorStand.getLocation().distanceSquared(player.getLocation()) < 25) {
                    player.removePotionEffect(PotionEffectType.REGENERATION);
                    // do not give regen if on fire (too close to fire)
                    if (player.getFireTicks() < 1) {
                        player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 120, 0, true, false)); // 5 seconds of regen
                    }
                }
            }
        }
    }
}
