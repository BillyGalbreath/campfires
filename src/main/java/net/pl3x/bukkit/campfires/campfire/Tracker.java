package net.pl3x.bukkit.campfires.campfire;

import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Tracker {
    private final Map<ArmorStand, ArmorStand> campfires = new HashMap<>();

    public void add(ArmorStand armorStand) {
        campfires.put(armorStand, addBase(armorStand));
    }

    public void remove(ArmorStand armorStand) {
        removeBase(armorStand);
        campfires.remove(armorStand);
    }

    public boolean contains(Location location) {
        for (ArmorStand armorStand : campfires.keySet()) {
            if (armorStand.getWorld().getName().equals(location.getWorld().getName()) &&
                    armorStand.getLocation().getBlockX() == location.getBlockX() &&
                    armorStand.getLocation().getBlockX() == location.getBlockX() &&
                    armorStand.getLocation().getBlockX() == location.getBlockX()) {
                return true;
            }
        }
        return false;
    }

    public Set<ArmorStand> getCampfires() {
        return campfires.keySet();
    }

    public void clear() {
        campfires.keySet().forEach(this::removeBase);
        campfires.clear();
    }

    private ArmorStand addBase(ArmorStand armorStand) {
        ArmorStand base = (ArmorStand) armorStand.getWorld().spawnEntity(armorStand.getLocation().add(0, -0.11, 0), EntityType.ARMOR_STAND);
        base.addPassenger(armorStand);
        base.setGravity(false);
        base.setMarker(true);
        base.setSmall(true);
        base.setVisible(false);
        return base;
    }

    private void removeBase(ArmorStand armorStand) {
        armorStand.leaveVehicle();
        ArmorStand base = campfires.get(armorStand);
        if (base != null) {
            armorStand.teleport(base.getLocation().add(0, 0.11, 0));
            base.remove();
        }
    }
}
